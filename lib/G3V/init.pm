package G3V::init;

use strict;
use warnings;

use Getopt::Long qw(GetOptionsFromArray);

use parent 'G3V';
use Carp;

sub make_track{
    my ($g3v, $arg, $extra_args) = @_;

    my $extra = {};
    GetOptionsFromArray($extra_args, $extra,
                        "margin=f{2}",
                        "title_size=f"
        );

    $extra->{title_size} ||= 20;
    $extra->{margin} ||= $g3v->{metadata}->{margin};

    $arg->{input} ||= pop(@$extra_args);

    croak "Must specify a range" unless $arg->{input};

    $g3v->{metadata}->{graph}->{width} = $arg->{width} - 2*$extra->{margin}->[0] if($arg->{width});
    $g3v->{metadata}->{graph}->{height} = $extra->{margin}->[1];
    $g3v->{metadata}->{margin} = $extra->{margin};

    $arg->{input} =~ /(.+):([0-9]+)-([0-9]+)/;
    @{$g3v->{metadata}->{range}}{qw/chr start stop/} = ($1, $2, $3);



    my $content = $g3v->g(id=>"main_content",
                          transform=>sprintf("translate(%f, %f)", @{$g3v->{metadata}->{margin}}));
    $content->text(x=>$g3v->{metadata}->{graph}->{width}/2,
               y=>$extra->{title_size},
               'text-anchor'=>"middle",
               'font-size'=>$extra->{title_size},
               -cdata=>$arg->{title}) if $arg->{title};


    $g3v->{metadata}->{graph}->{height} += $extra->{title_size};
}

1;
