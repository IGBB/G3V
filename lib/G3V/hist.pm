package G3V::hist;

use strict;
use warnings;

use parent 'G3V';
use Carp;

use POSIX qw(ceil);

sub roundup
{
  my $num = shift;
  my $roundto = shift || 1;

  return int(ceil($num/$roundto))*$roundto;
}

sub calc_step{

  my ($min, $max, $breaks) = @_;
  my $length = $max - $min;

  # /* Find a tick step that is
    #  * - at least 5
    #  * - divisible by 5,
    #  * - creates 5 ticks */
  my $step = int($length/$breaks);
  $step = int($step /5 )*5;
  $step = 5 if ($step < 5) ;

  return $step;
}



sub make_track{
    my ($g3v, $arg) = @_;
    my ($width, $height) = @{$g3v->{metadata}->{graph}}{qw/width height/};
    my $range = $g3v->{metadata}->{range};
    my $track = $g3v->{metadata}->{track};
    $track->{height} = $arg->{height} || 50;

    my $region_length = $range->{stop} - $range->{start} + 1;


    # Load histograph from bga file
    my $hist = [map {[$_,0]} ($range->{start} .. $range->{stop})];
    my $max = 0;  
    my $min = 'inf';
    open(my $fh, $arg->{input});
    while(<$fh>){
        chomp;
        my $line = {};
        @{$line}{qw/ chr start stop value /} = split;
        next unless ($line->{chr} eq $range->{chr} &&
                     (($line->{start} >= $range->{start} && $line->{start} <= $range->{stop}) ||
                      ($line->{stop}  >= $range->{start} && $line->{stop}  <= $range->{stop})));

        $line->{start} = $range->{start} if($line->{start} < $range->{start});
        $line->{stop}  = $range->{stop}  if($line->{stop}  > $range->{stop} );
        
        $max = $line->{value} if($max < $line->{value});
        $min = $line->{value} if($min > $line->{value});
        foreach my $cur ($line->{start} .. $line->{stop}){
            $hist->[$cur-$range->{start}]->[1] = $line->{value};
        }
    }
    close $fh;

    #Point compression ( remove intermediate points that form a horizontal line )
    my $cur = 0;
    while($cur < (scalar(@$hist)-2)){    
        if($hist->[$cur]->[1] == $hist->[$cur+1]->[1] &&
           $hist->[$cur]->[1] == $hist->[$cur+2]->[1]){
            splice(@$hist, $cur+1, 1);
        }else{
            $cur++;
        }
    }

    my $group = $g3v->getElementByID('main_content')->g(transform => "translate(0,$height)");

    #TRACK Title
    $group->text(x=>$track->{title} , y=>$track->{height}/2,
                 'text-anchor'=>"end",
                 -cdata=>$arg->{title}) if $arg->{title};

    #TRACK CONTENT
    my $real_max = $max;
    $max = 1 if $max < 1;
    $max = roundup($max, 10);

    my $content = $group->g( transform => sprintf("translate(%f, 0)",
                                                  $track->{title} + $track->{pad}));

    # $content->rect(
    #     x=>$g3v->{start},
    #     y=>0,
    #     width=>'100%',
    #     height=>'100%',
    #     fill=>'#ddd');
    unshift(@$hist, [$range->{start}, 0]);
    push(@$hist, [$range->{stop}, 0]);
    
    my $points = $content->get_path(
    x       => [map {($_->[0] - $range->{start})/$region_length * $track->{content} } @$hist],
    y       => [map {($max - $_->[1])/$max * $track->{height}} @$hist],
    -type   => 'polyline',
    -closed => 'true' #specify that the polyline is closed.
    );
    $content->polyline (
        %$points,
        style => {
          'fill-opacity' => 1,
              'stroke-width' => 0.5,
              'fill' => "#DDD"
        },
    );


    # $track->line(
    #     id=>"yaxis",
    #     x1=>$g3v->{width}/4, x2=>$g3v->{width}/4,
    #     y1=>0, y2=>$track_height,
    #     style => {
    #       'stroke-width' => 1,
    #           'stroke-opacity' => 1,
    #           'stroke' => '#888'
    #     });



    my $step = calc_step(0, $max, 4);
    for (my $i=$step; $i <= $max; $i += $step) {
      my $y = ($max - $i)/$max * $track->{height};

      $content->line(
        x1=>0, x2=>$track->{content},
        y1=>$y, y2=>$y,
        'stroke-dasharray' => "1 9",
        style => {
          'stroke-width' => 1,
              'stroke-opacity' => 0.5,
              'stroke' => 'white'
        });

      $content->line(
        x1=>0, x2=>-5,
        y1=>$y, y2=>$y,
        style => {
          'stroke-width' => 1,
              'stroke-opacity' => 1,
              'stroke' => '#888'
        });

      $content->text(
                   x=>-6,
                   y=>$y + 4,
                   'text-anchor'=>"end",
                   'font-size'=>"8px",
                   -cdata=>$i);


    }

    $g3v->{metadata}->{graph}->{height} += $track->{height};
}

1;
