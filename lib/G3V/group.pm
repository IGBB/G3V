package G3V::group;

use strict;
use warnings;

use parent 'G3V';
use Carp;

use Getopt::Long qw(GetOptionsFromArray);

use Data::Dumper;

sub make_track{
    my ($g3v, $arg, $extra_args) = @_;

    my $extra = {};
    GetOptionsFromArray($extra_args, $extra,
                        "group=s",
                        "stroke=s",
                        "stroke_width=f",
                        "title_size=f"
        );

    $arg->{input} ||= pop(@$extra_args);

    $extra->{stroke} ||= "black";
    $extra->{stroke_width} ||= 2;
    $extra->{title_size}   ||= 16;
    $extra->{group}  ||= "group" . scalar @{$g3v->{'-document'}->{'-childs'}};

    my ($width, $height) = @{$g3v->{metadata}->{graph}}{qw/width height/};

    my $track_height = $arg->{height} || $extra->{title_size};
    
    my $track = $g3v->getElementByID('main_content')->g(id=>$extra->{group},
                                                         transform => "translate(0,$height)");

    #TRACK Title
    $track->text(x=>5, y=>$track_height-6, 'font-size'=>$extra->{title_size},
                 -cdata=>$arg->{title}) if $arg->{title};
    $track->line(x1=>0, x2=>$width,
                 y1=>$track_height-5, y2=>$track_height-5,
                 stroke => $extra->{stroke}, stroke_width=>$extra->{stroke_width} ) ;


    $g3v->{metadata}->{graph}->{height} += $track_height;

}

1;
