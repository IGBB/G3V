package G3V::region;

use strict;
use warnings;

use List::Util qw/first/;
use Getopt::Long qw(GetOptionsFromArray);

use parent 'G3V';
use Carp;

sub make_track{
    my ($g3v, $arg, $extra_args) = @_;

    my $extra = {};
    GetOptionsFromArray($extra_args, $extra,
                        "group=s",
                        "stroke=s",
                        "stroke_width=f",
                        "title_size=f"
        );

    my ($width, $height) = @{$g3v->{metadata}->{graph}}{qw/width height/};
    my $range = $g3v->{metadata}->{range};
    my $track = $g3v->{metadata}->{track};
    $track->{height} = $arg->{height} || 20;


    my $group = $g3v->getElementByID('main_content')->g(transform => "translate(0,$height)");

    #TRACK Title
    $group->text(x=>$track->{title} , y=>$track->{height}/2,
                 'text-anchor'=>"end",
                 -cdata=>$arg->{title}) if $arg->{title};

    #TRACK CONTENT
    my $content = $group->g( transform => sprintf("translate(%f, 0)",
                                                  $track->{title} + $track->{pad}));


    my $regions = [];
    open(my $fh, $arg->{input});
    while(<$fh>){
        chomp;
        my $line = {};
        @{$line}{qw/ chr start stop name /} = split;
        $line->{strand} = ($line->{start} < $line->{stop})?'sense':'anti';
        @$line{qw/start stop/} = sort {$a <=> $b} @$line{qw/start stop/};
        $line->{width} = $line->{stop} - $line->{start};
        
        next unless ($line->{chr} eq $range->{chr} &&
                     (($line->{start} >= $range->{start} && $line->{start} <= $range->{stop}) ||
                      ($line->{stop}  >= $range->{start} && $line->{stop}  <= $range->{stop})));

        push(@$regions, $line);
    }
    close $fh;

    @$regions = sort {$a->{start} <=> $b->{start}} @$regions;
    my @subtrack = ();

    my $scale = $track->{content}/($range->{stop} - $range->{start} + 1);
    for my $cur (@$regions) {
        # Get index of first track with available space
        # Create subtrack in no space available
        my $i = first {($subtrack[$_] + 10/$scale) < $cur->{start}} (0 .. $#subtrack);
        $i = scalar @subtrack unless defined $i;


        $content->rectangle( x => ($cur->{start}-$range->{start})*$scale,
                        y => $i*$track->{height},
                        width => ($cur->{stop}-$cur->{start})*$scale,
                        height => $track->{height}-5);

        $subtrack[$i] = $cur->{stop};
    }


    $g3v->{metadata}->{graph}->{height} += $track->{height} * scalar(@subtrack);
}

1;
