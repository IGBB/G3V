package G3V::de;

use strict;
use warnings;

use parent 'G3V';
use Carp;

use Data::Dumper;

sub make_track{
    my ($g3v, $arg) = @_;

    my $track_height = $arg->{height} || 20;
    
    my $track = $g3v->svg(y=>$g3v->{height}, height=>$track_height);

    open(my $fh, $arg->{input});
    while(<$fh>){
        chomp;
        my $line = {};
        @$line{qw/id fc cpm lk pval fdr stuff/} = split;

        $line->{id} =~ s/"([^"]*)"/$1/;
        
        my $elem = $g3v->getElementByID($line->{id});
        next unless ($elem && $line->{fdr} <= 0.05);

        $elem->{style}->{stroke} = ($line->{fc} >= 0)?'green':'red';
        
    }
    close $fh;
}

1;
