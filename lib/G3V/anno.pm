package G3V::anno;

use strict;
use warnings;

use Getopt::Long qw(GetOptionsFromArray);

use parent 'G3V';
use Carp;

sub make_track{
    my ($g3v, $arg, $extra_args) = @_;

    my $extra = {};
    GetOptionsFromArray($extra_args, $extra,
                        "group=s",
                        "stroke=s",
                        "stroke_width=f",
                        "title_size=f"
        );

    my ($width, $height) = @{$g3v->{metadata}->{graph}}{qw/width height/};
    my $range = $g3v->{metadata}->{range};
    my $track = $g3v->{metadata}->{track};
    $track->{height} = $arg->{height} || 20;


    my $group = $g3v->getElementByID('main_content')->g(transform => "translate(0,$height)");

    my $top = [];
    my $annotation = {};
    open(my $fh, $arg->{input});
    while(<$fh>){
        next if /^#/;
        chomp;
        my $line = {};
        @$line{qw/chr source feature start stop score strand frame attr/} = split("\t", $_, 9);

        next unless (defined($line->{chr}) && $line->{chr} eq $range->{chr} &&
                     (($line->{start} >= $range->{start} && $line->{start} <= $range->{stop}) ||
                      ($line->{stop}  >= $range->{start} && $line->{stop}  <= $range->{stop})));
        
        $line->{attr} = {map {split /=/, $_, 2} split( /;/, $line->{attr})};
        
        my $id = $line->{attr}->{ID};
        
        $annotation->{$id} ||= {};
        @{$annotation->{$id}}{qw/feature start stop strand/} = @$line{qw/feature start stop strand/};
        @{$annotation->{$id}}{qw/name ID/} = @{$line->{attr}}{qw/Name ID/};
        
        $annotation->{$id}->{width} = $annotation->{$id}->{stop} - $annotation->{$id}->{start};
        
        push(@$top, $id) unless $line->{attr}->{Parent};
        
        if($line->{attr}->{Parent}){
            my $parent = $line->{attr}->{Parent};
            $annotation->{$parent} ||= {};
            $annotation->{$parent}->{children} ||= {};
            $annotation->{$parent}->{children}->{$line->{feature}} ||= [];
            push(@{$annotation->{$parent}->{children}->{$line->{feature}}}, $id);
        }
    }
    close $fh;
    @$top = sort {$annotation->{$a}->{start} <=> $annotation->{$b}->{start}} @$top;
    
    #TRACK Title
    $group->text(x=>$track->{title} , y=>$track->{height}/2,
                 'text-anchor'=>"end",
                 -cdata=>$arg->{title}) if $arg->{title};

    #TRACK CONTENT
    my $content = $group->g( transform => sprintf("translate(%f, 0)",
                                                  $track->{title} + $track->{pad}));

    my $defs = $content->defs(id  =>  'ruler_defs',);
    $defs->marker(  id     => "anno_direction",
                    markerWidth  => 10,
                    markerHeight => 10,
                    refY=>3, 
                    refX=>0, 
                    orient=>"auto", 
                    markerUnits=>"strokeWidth")->path(d=>"M0,0 L0,6 L9,3 z", stroke=>'black');
    my $cur_height = 0;
    my $scale = $track->{content}/($range->{stop} - $range->{start} + 1);
    while(grep {$_} @$top){
        my $cur = undef;
        foreach my $id (grep {$_} @$top){
            next if ($cur && $annotation->{$id}->{start} < $cur->{stop}+10);

            $cur = $annotation->{$id};

            my $gene = $content->tag('g',
                                     id=>$id,
                                      style => {
                                          stroke => 'gray',
                                      },
                );


            my $y = $cur_height+($track->{height}/2);

            #DRAW mrna
            $gene->line( x1 => ($cur->{start}-$range->{start})*$scale, y1 => $y ,
                          x2 => ($cur->{stop}-$range->{start})*$scale,  y2 => $y ,
                            'stroke-width'=>(($track->{height}-5)/4));
            #DRAW exons
            foreach my $child (map {$annotation->{$_}} @{$cur->{children}->{exon}}){
                $gene->line( x1 => ($child->{start}-$range->{start})*$scale, y1 => $y ,
                             x2 => ($child->{stop}-$range->{start})*$scale,  y2 => $y ,
                             'stroke-width'=>(($track->{height}-5)/2));
            }
            #DRAW cds
            # foreach my $child (map {$annotation->{$_}} @{$cur->{children}->{CDS}}){
            #     $gene->line( x1 => ($child->{start}-$range->{start})*$scale, y1 => $y ,
            #                  x2 => ($child->{stop}-$range->{start})*$scale,  y2 => $y ,
            #                  'stroke-width'=>(($track->{height}-5)));
            # }

            $id = undef;
        }
        $cur_height += $track->{height};
    }

    $g3v->{metadata}->{graph}->{height} += $cur_height;
}

1;
