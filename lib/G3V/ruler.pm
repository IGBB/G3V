package G3V::ruler;

use strict;
use warnings;

use parent 'G3V';
use Carp;

use POSIX qw(ceil);

sub log10 {
    my $n = shift;
    return log($n)/log(10);
}

sub roundup
{
  my $num = shift;
  my $roundto = shift || 1;

  $roundto = 10 ** ($roundto - 1);

  return int(ceil($num/$roundto))*$roundto;
}

sub rounddown
{
  my $num = shift;
  my $roundto = shift || 1;

  $roundto = 10 ** ($roundto - 1);


  return int($num/$roundto)*$roundto;
}

sub calc_step{

  my ($min, $max, $breaks) = @_;
  my $length = $max - $min;

  # /* Find a tick step that is
    #  * - at least 5
    #  * - divisible by 5,
    #  * - creates 5 ticks */
  my $step = int($length/$breaks);
  $step = int($step /5 )*5;
  $step = 5 if ($step < 5) ;

  return $step;
}


sub make_track{
    my ($g3v, $arg) = @_;

    my ($width, $height) = @{$g3v->{metadata}->{graph}}{qw/width height/};

    my $range = $g3v->{metadata}->{range};
    $range->{length} = $range->{stop} - $range->{start} + 1;

    my $track = $g3v->{metadata}->{track};
    $track->{height} = $arg->{height} || 20;

    my $group = $g3v->getElementByID('main_content')->g(transform => "translate(0,$height)");

    #TRACK Title
    $group->text(x=>$track->{title} , y=>$track->{height}/2,
                 'text-anchor'=>"end",
                 'dominant-baseline'=>'central',
                 -cdata=>$range->{chr});

    #TRACK CONTENT
    my $content = $group->g( transform => sprintf("translate(%f, 0)",
                                                  $track->{title} + $track->{pad}));




    my $log = int(log10($range->{length}));
    my $start = roundup($range->{start},  $log);
    my $stop  = rounddown($range->{stop}, $log);
    my $step = calc_step($start, $stop, 10);


    my $y = $track->{height}/2;
    for(my $i = $start; $i <= $stop; $i+=$step){

        my $x = ($i - $range->{start} ) * $track->{content} / $range->{length};

        $content->line(
            x1=>$x, x2=>$x,
            y1=>$y-2, y2=>$y+2,
            style => {
                'stroke-width' => 1,
                    'stroke-opacity' => 1,
                    'stroke' => '#888'

            });

        $content->text(
                   x=>$x,
                   y=>$y - 4,
                   'text-anchor'=>"middle",
                   'font-size'=>"8px",
                   -cdata=>$i);


    }



    $g3v->{metadata}->{graph}->{height} += $track->{height};
}

1;
