package G3V;

use strict;
use warnings;

use SVG::Parser;
use XML::Hash::XS;

use parent 'SVG';

local $\ = "\n";

sub _set_var {
    my $self = shift;
    $self->{'-nocredits'} =  1;


    my $metadata = $self->getElementByID("g3v-metadata");
    if($metadata){
        $self->{metadata} = xml2hash $metadata->xmlify;
    }else{
        $self->{metadata} = {
            id => 'g3v-metadata',
            range => {chr => "", start=>0, stop=>0},
            margin => [50,10],
            graph  => {width => 800, height => 5},
            track => {content => 690, title=> 90, pad => 20 }
        };

    }


}

sub new {
    my ($proto)=@_;
    my $class=ref $proto || $proto;
    my $self = $class->SUPER::new(height=>5,width=>900);
    $self->_set_var();

    return $self;
}

sub load {
    my ($proto,$fh)=@_;
    my $class=ref $proto || $proto;
    my $self;

    eval {
        $self = SVG::Parser->new()->parsefile($fh);
        bless $self, $class;

        $self->_set_var();
    } or do {
        $self = $class->new();
    };
    
    return $self;
}

sub DESTROY {
    my $self = shift;
    my $svg = $self->{"-document"};
    my $metadata = $self->{metadata};

    $metadata->{graph}->{height} += 5;

    my $mde = $self->getElementByID("g3v-metadata");
    $svg->removeChild($mde) if $mde;

    $svg->cdata_noxmlesc("\n".
        hash2xml( $metadata,
                  xml_decl=>0,
                  root=>'metadata',
                  use_attr  => 1,
                  indent => 1)
        );

    $svg->attr("height", $metadata->{graph}->{height} + $metadata->{margin}->[1]);
    $svg->attr("width", $metadata->{graph}->{width} + 2*$metadata->{margin}->[0]);

    print $self->xmlify;
}


1;
