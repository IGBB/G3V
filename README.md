## G3V
Grammar Graphics for a Genome Viewer

### Installation

``` sh
git clone https://gitlab.com/IGBB/G3V.git
cpanm -l G3V/ SVG::Parser XML::Hash::XS
```

### Usage

``` sh
g3v init -i $region -t $gene |
    g3v anno -t Annotations -i B1.gff3 |
    g3v group -g "B1" -t "Gossypium anomalum" |
    g3v ruler |
    g3v hist -t "SRR3560153"  -i SRR3560153.bed |
    g3v hist -t "SRR3560155"  -i SRR3560155.bed |
    g3v hist -t "SRR12745560" -i SRR12745560.bed |
    g3v group -g "D3" -t "Gossypium davidsonii" |
    g3v ruler |
    g3v hist -t "SRR6334584" -i SRR6334584.bed |
    g3v hist -t "SRR8136261" -i SRR8136261.bed |
    g3v group -g "A2" -t "Gossypium arboreum" |
    g3v ruler |
    g3v hist -t "SRR8979965" -i SRR8979965.bed |
    g3v hist -t "SRR8979944" -i SRR8979944.bed |
    g3v hist -t "SRR8979925" -i SRR8979925.bed |
    g3v group -g "E2" -t "Gossypium somalense" |
    g3v ruler |
    g3v hist -t "SRR3560162" -i SRR3560162.bed |
    g3v group -g "F1" -t "Gossypium longicalyx" |
    g3v ruler |
    g3v hist -t "SRR617704"  -i SRR617704.bed |
    convert svg:- $gene.g3v.png
```

#### `g3v init`

Initializes the g3v svg setting the title (`-t`) and region (`-i`) in the formate `contig:start-stop`

``` sh
g3v init -i NC_000007.14:155799980-155812463  -t "SHH (human)" 
```

![](img/init.svg)

#### `g3v ruler`

Adds a ruler for the region of interest. 

``` sh
g3v init -i NC_000007.14:155799980-155812463  -t "SHH (human)" |
    g3v ruler 
```

![](img/ruler.svg)

#### `g3v anno`

Adds annotation layer using the supplied gff3 file. 

``` sh
g3v init -i NC_000007.14:155799980-155812463  -t "SHH (human)" |
    g3v ruler  |
    g3v anno -t Annotations -i GCF_000001405.39_GRCh38.p13_genomic.gff 
```

![](img/anno.svg)

#### `g3v region`

Adds layer with highlighted regions from supplied bed file. 

``` sh
g3v init -i NC_000007.14:155799980-155812463  -t "SHH (human)" |
    g3v ruler  |
    g3v region -t Regions -i test.bed
```




### Contribution Guide

*TODO*
