#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;
use SVG::Parser;
use SVG;

use Data::Dumper;

my $xml;
{
    local $/=undef;
    $xml=<>;
}

my $svg=new SVG::Parser()->parse($xml);

my ($id,$width,$height) = @{$svg->{'-document'}}{qw/id width height/};
$height+=10;

$id =~ /(.+):([0-9]+)-([0-9]+)/;
my ($chr,$start,$stop) = ($1, $2, $3);

#ADD CODE HERE


$height += 20;

$svg->{'-document'}->{height} = $height;

print $svg->xmlify;
